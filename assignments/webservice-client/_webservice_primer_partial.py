import requests
import json

class _webservice_primer:
    def __init__(self): # constructor, self is imp
        self.SITE_URL = 'http://student04.cse.nd.edu:51001'
        self.MOVIES_URL = self.SITE_URL + '/movies/'
        self.RESET_URL = self.SITE_URL + '/reset/'

    def get_movie(self, mid):
        r = requests.get(self.MOVIES_URL + str(mid))
        resp = json.loads(r.content)
        return(resp)

    def reset_movie(self, mid):
        movie_data = {} # empty dictionary, to pass in request body
        movie_string = json.dumps(movie_data)
        r = requests.put(self.RESET_URL + str(mid), data=movie_string) #a request with with a message body as data
        resp = json.loads(r.content.decode('utf-8'))
        return(resp)

    def display_movie(self, mid):
        movie = self.get_movie(mid)
        if movie['result'] == 'success':
            print("Title: " + movie['title'])
        else:
            print("Error: " + movie['message'])

if __name__ == "__main__":
    MID = XX # change this to your assigne MID
    ws = _webservice_primer()
    print(ws.get_movie(MID))
    ws.display_movie(MID)
