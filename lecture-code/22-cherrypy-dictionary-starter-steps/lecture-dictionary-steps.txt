# lecture dictionary server steps:

# step 1: show the most basic server
import cherrypy

def start_service():
    '''configures and runs the server'''

    # configuration for the server
    conf = {
            'global' : {
                'server.socket_host' : 'student04.cse.nd.edu', # or use localhost, or student05, or student10/11/12/13
                'server.socket_port' : <YOUR PORT NUM>,
                },
            #'/' : {}
            }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf) # creates the app
    cherrypy.quickstart(app)


if __name__ == '__main__':
    start_service()

# step 2: run this server and access it on the browser
# you can see that the server is accessible, but does not even have '/' path defined.
$ python3 server.py
[28/Sep/2020:12:42:46] ENGINE Listening for SIGTERM.
[28/Sep/2020:12:42:46] ENGINE Listening for SIGHUP.
[28/Sep/2020:12:42:46] ENGINE Listening for SIGUSR1.
[28/Sep/2020:12:42:46] ENGINE Bus STARTING
[28/Sep/2020:12:42:46] ENGINE Started monitor thread 'Autoreloader'.
[28/Sep/2020:12:42:46] ENGINE Started monitor thread '_TimeoutMonitor'.
[28/Sep/2020:12:42:46] ENGINE Serving on http://student04.cse.nd.edu:51005
[28/Sep/2020:12:42:46] ENGINE Bus STARTED
10.63.0.119 - - [28/Sep/2020:12:43:00] "GET / HTTP/1.1" 404 1302 "" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36"
10.63.0.119 - - [28/Sep/2020:12:43:01] "GET /favicon.ico HTTP/1.1" 404 1324 "http://student04.cse.nd.edu:51005/" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36"
^C[28/Sep/2020:12:44:11] ENGINE Keyboard Interrupt: shutting down bus
[28/Sep/2020:12:44:11] ENGINE Bus STOPPING
[28/Sep/2020:12:44:11] ENGINE HTTP Server cherrypy._cpwsgi_server.CPWSGIServer(('student04.cse.nd.edu', 51005)) shut down
[28/Sep/2020:12:44:11] ENGINE Stopped thread '_TimeoutMonitor'.
[28/Sep/2020:12:44:11] ENGINE Stopped thread 'Autoreloader'.
[28/Sep/2020:12:44:11] ENGINE Bus STOPPED
[28/Sep/2020:12:44:11] ENGINE Bus EXITING
[28/Sep/2020:12:44:11] ENGINE Bus EXITED
[28/Sep/2020:12:44:11] ENGINE Waiting for child threads to terminate...
# in the browser it shows
404 Not Found
The path '/' was not found.

Traceback (most recent call last):
  File "/escnfs/home/csesoft/2017-fall/anaconda3/lib/python3.6/site-packages/cherrypy/_cprequest.py", line 670, in respond
    response.body = self.handler()
  File "/escnfs/home/csesoft/2017-fall/anaconda3/lib/python3.6/site-packages/cherrypy/lib/encoding.py", line 217, in __call__
    self.body = self.oldhandler(*args, **kwargs)
  File "/escnfs/home/csesoft/2017-fall/anaconda3/lib/python3.6/site-packages/cherrypy/_cperror.py", line 411, in __call__
    raise self
cherrypy._cperror.NotFound: (404, "The path '/' was not found.")
Powered by CherryPy 3.8.0

# step 3: add a dispatcher to connect endpoints to code in server.py
import cherrypy
import routes # add import

def start_service():
    '''configures and runs the server'''

    # create dispatcher
    dispatcher = cherrypy.dispatch.RoutesDispatcher() # dispatcher object
    # we will use this to connect endpoints to controllers

    # configuration for the server
    conf = {
            'global' : {
                'server.socket_host' : 'student04.cse.nd.edu',
                'server.socket_port' : 51005, # hands off, don't use mine
                },
            '/' : { # add dispatcher to conf
                'request.dispatch' : dispatcher, # our dispatcher object
                }
            }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf) # creates the app
    cherrypy.quickstart(app)


if __name__ == '__main__':
    start_service()


# step 4: create a controller to house event handlers - functions
import cherrypy
import json

class DictionaryController(object):
    # this is a controller class, which holds event handlers
    # constructor
    def __init__(self):
        self.myd = dict()

    def get_value(self, key):
        return self.myd[key]

    # event handlers for resource requests
    def GET_KEY(self, key):
        #TODO
        pass

    def PUT_KEY(self, key):
        #TODO
        pass

# step 5: write handlers
# in dictionary_controller.py
def get_value(self, key):
        return self.myd[key]

    def add_entry(self, key, value):
        self.myd[key] = value

    # event handlers for resource requests
    def GET_KEY(self, key):
        output = {'result' : 'success'} # 1.
        key = str(key) # 2.

        try:
            house = self.get_value(key)
            if house is not None:
                output['key'] = key
                output['value'] = house
            else:
                output['result'] = 'error'
                output['message'] = 'None type value associated with requested key'
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)


    def PUT_KEY(self, key):
        # 1. create a default response object
        output = {'result' : 'success'}
        key = str(key) # 2. cast the input into the desired type
        # 3. (only for body)get body of message
        data_json = json.loads(cherrypy.request.body.read())

        # 4. do the work in try-except block(s)
        try:
            val = data_json['value'] # grab the value in the message
            self.add_entry(key, val) # actual update happens here
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        # 5. return the string response
        return json.dumps(output)

    def GET_INDEX(self):
        #TODO
        pass

    def POST_INDEX(self):
        #TODO
        pass

    def DELETE_KEY(self, key):
        #TODO
        pass

    def DELETE_INDEX(self):
        output = {'result': 'success'}
        self.myd = dict()
        return json.dumps(output)


# step 6:  connect event handlers to server through dispatcher
# in server.py
from dictionary_controller import DictionaryController # getting our controller class

def start_service():
    '''configures and runs the server'''
    dCon = DictionaryController() # create controller object

    dispatcher = cherrypy.dispatch.RoutesDispatcher() # dispatcher object
    # we will use this to connect endpoints to controllers

    # connecting endpoints to resources
    #connect(out_tag, http resource, class object with handler, event handler name, what type of HTTP request to serve)
    dispatcher.connect('dict_get_key', '/dictionary/:key', controller=dCon, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('dict_put_key', '/dictionary/:key', controller=dCon, action='PUT_KEY', conditions=dict(method=['PUT']))

# step 7: now, test your server by first running your server, and then any of these options:
# option1 : use browser or REST client to send the correct request
# option2 : use the test file provided. First fix the port number and machine number on it. Note, the test file depends on some functions to work to test other functions, e.g. DELETE_INDEX
# option3 :
